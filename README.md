# Php fpm

Based on **php:7.3-fpm-alpine** image with some external modules:
- curl
- fpm
- gd
- iconv
- pdo_mysql
- pdo_pgsql
- xdebug (configured for fpm only)
- zip

