#! /bin/sh

BASE_DIR="/var/www/html/.docker/php/mail"
TARGET="${BASE_DIR}/log/$(TZ="Asia/Novosibirsk" date +%Y%m%d_%H%M%S).log"

mkdir -p $(dirname ${TARGET})

cat /dev/fd/0 > ${TARGET};

EMAIL_TO=$(cat "${TARGET}" | grep -i 'To: ' | head -n1 | cut -d' ' -f2-100)

if [[ -n "${EMAIL_TO}" ]]; then
    DIR_TO="${BASE_DIR}/to/${EMAIL_TO}"

    mkdir -p ${DIR_TO}

    ln -s ../../log/$(basename ${TARGET}) ${DIR_TO}/
fi

exit 0
