FROM php:7.3-fpm-alpine

ENV PHP_REDIS_VERSION 4.3.0
ENV PHP_XDEBUG_VERSION 2.7.1

RUN set -xe \
    && apk add --no-cache --virtual .persistent-deps \
        file \
        icu-dev \
        postgresql-dev \
        libxml2-dev \
        libressl-dev \
        freetype \
        libpng \
        libjpeg-turbo \
        msmtp \
        zip \
        libzip \
        libzip-dev \
    && apk add --no-cache --virtual .build-deps \
        autoconf \
        cmake \
        g++ \
        gcc \
        libc-dev \
        pcre-dev \
        make \
        git \
        pkgconf \
        re2c \
        freetype-dev \
        libpng-dev  \
        libjpeg-turbo-dev \
    && docker-php-ext-configure gd \
        --with-gd \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure bcmath --enable-bcmath \
    && docker-php-ext-configure intl --enable-intl \
    && docker-php-ext-configure mbstring --enable-mbstring \
    && docker-php-ext-configure mysqli --with-mysqli \
    && docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql \
    && docker-php-ext-configure pdo_pgsql --with-pgsql \
    && docker-php-ext-configure soap --enable-soap \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install -j$(nproc) \
        bcmath \
        iconv \
        intl \
        gd \
        mbstring \
        mysqli \
        opcache \
        pcntl \
        pdo_mysql \
        pdo_pgsql \
        soap \
        zip \
    #
    # xdebug
    #
    && pecl install xdebug-${PHP_XDEBUG_VERSION} \
    #
    # redis
    #
    && git clone --branch ${PHP_REDIS_VERSION} https://github.com/phpredis/phpredis /tmp/phpredis \
    && cd /tmp/phpredis \
    && phpize  \
    && ./configure  \
    && make  \
    && make install \
    && make test \
    #
    # cleanup
    #
    && apk del .build-deps \
    && rm -rf /tmp/*

COPY config/php-cli.ini /usr/local/etc/php/php.ini
COPY config/php-fpm.ini /usr/local/etc/php/php-fpm.ini
COPY config/fpm/php-fpm.conf /usr/local/etc/
COPY config/fpm/pool.d /usr/local/etc/pool.d
COPY bin/sendmail-local.sh /usr/local/bin/sendmail-local.sh
COPY docker-entrypoint.sh /docker-entrypoint.sh

WORKDIR /var/www/html

VOLUME ["/var/www/html"]

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["php-fpm", "--nodaemonize", "-c", "/usr/local/etc/php/php-fpm.ini", "--fpm-config", "/usr/local/etc/php-fpm.conf"]

EXPOSE 9000
