#! /usr/bin/env sh

set -ex

umask 0002

exec "$@"
